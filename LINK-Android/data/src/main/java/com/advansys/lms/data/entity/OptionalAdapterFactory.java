package com.horus.link.data.entity;

import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class OptionalAdapterFactory<E> extends TypeAdapter<Optional<E>> {

    private final TypeAdapter<E> adapter;

    public OptionalAdapterFactory(TypeAdapter<E> adapter) {
        this.adapter = adapter;
    }

    public static TypeAdapterFactory create() {
        return new TypeAdapterFactory() {
            @Override
            public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
                Class<T> rawType = (Class<T>) type.getRawType();
                if (rawType != Optional.class) {
                    return null;
                }
                final ParameterizedType parameterizedType = (ParameterizedType) type.getType();
                final Type actualType = parameterizedType.getActualTypeArguments()[0];
                final TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(actualType));
                return new OptionalAdapterFactory(adapter);
            }
        };
    }

    @Override
    public void write(JsonWriter out, Optional<E> value) throws IOException {
        if (value.isPresent()) {
            this.adapter.write(out, value.get());
        } else {
            out.nullValue();
        }
    }

    @Override
    public Optional<E> read(JsonReader in) throws IOException {
        final JsonToken peek = in.peek();
        if (peek != JsonToken.NULL) {
            return Optional.fromNullable(this.adapter.read(in));
        }
        return Optional.<E>absent();
    }
}
