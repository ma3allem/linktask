package com.horus.link.data.entity;

import com.google.common.base.Optional;

import java.util.List;

public interface ApiResponse {

    Optional<String> status();

    Optional<String> message();

    Optional<String> error();

    Optional<List<Error>> errors();

    interface Error {

        Optional<Integer> code();

        Optional<String> fields();

        Optional<String> message();
    }

    interface Creator<T> {

        T status(Optional<String> status);

        T message(Optional<String> message);

        T error(Optional<String> error);

        T errors(Optional<List<Error>> errors);
    }
}
