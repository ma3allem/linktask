package com.advansys.lms.data.net.api;

import com.advansys.lms.domain.model.articles.ArticlesList;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ArticlesApiService {

    @GET("/v1/articles")
    Single<Response<ArticlesList>> fetchArticlesA(@Query("source") String source, @Query("apiKey") String apiKey);

    @GET("/v1/articles")
    Single<Response<ArticlesList>> fetchArticlesB(@Query("source") String source, @Query("apiKey") String apiKey);
}
