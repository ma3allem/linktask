package com.horus.link.data.net.client;

import android.text.TextUtils;

import com.horus.link.data.cache.LocalPreference;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;

public class HttpClient {

    private LocalPreference preference;

    @Inject
    public HttpClient(LocalPreference preference) {
        this.preference = preference;
    }

    public OkHttpClient createOkHttpClient() {
        TrustManager[] trustAllCertificates = new TrustManager[]{new EasyTrustManager()};
        Interceptor interceptor = this.createOkHttpInterceptor();
        HostnameVerifier easyHostnameVerifier = new EasyHostnameVerifier();
        try {
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCertificates, new java.security.SecureRandom());
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            return new OkHttpClient.Builder()
                    .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCertificates[0])
                    .hostnameVerifier(easyHostnameVerifier)
                    .addInterceptor(interceptor)
                    .addInterceptor(new HttpLoggingInterceptor()
                            .setLevel(HttpLoggingInterceptor.Level.BODY))
                    .connectTimeout(15000, TimeUnit.MILLISECONDS)
                    .readTimeout(15000, TimeUnit.MILLISECONDS)
                    .build();
        } catch (Exception e) {
            return null;
        }
    }

    private Interceptor createOkHttpInterceptor() {
        return chain -> {
            String accept = chain.request().header("Accept");
            String authorization = chain.request().header("Authorization");
            if (!TextUtils.isEmpty(accept) && !TextUtils.isEmpty(authorization)) {
                return chain.proceed(chain.request());
            }

            Request.Builder newRequestBuilder = chain.request().newBuilder()
                    .removeHeader("Accept")
                    .removeHeader("Authorization")
                    .addHeader("Accept", "application/json");

            String cachedAuthorization = this.preference.getAuthorizationToken();
            if (!TextUtils.isEmpty(cachedAuthorization)) {
                newRequestBuilder.addHeader("Authorization", cachedAuthorization);
            }
            return chain.proceed(newRequestBuilder.build());
        };
    }
}
