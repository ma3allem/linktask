package com.advansys.lms.data.repository;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.advansys.lms.data.net.api.ArticlesApiService;
import com.advansys.lms.domain.model.articles.ArticlesList;
import com.advansys.lms.domain.repository.ArticlesRepository;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public final class ArticlesDataRepository implements ArticlesRepository {

    private ArticlesApiService articlesApiService;
    private ArticlesList articles;

    @Inject
    public ArticlesDataRepository(ArticlesApiService apiService) {
        this.articlesApiService = apiService;
    }

    private void cacheArticles(ArticlesList articles) {
        this.articles = articles;
    }

    private ArticlesList map(Response<ArticlesList> response) throws IOException {
        if (response == null) throw new IllegalStateException("Invalid lookups response");

        //LookupsEntity entity = response.body();
        if (response.body() == null) throw new IllegalStateException("Invalid articles response");

        switch (response.code()) {
            case 200:
                return response.body();
            default:
                if (TextUtils.isEmpty(response.errorBody().string())) {
                    throw new IllegalStateException("Fetch articles failure");
                }
                throw new IllegalStateException(response.errorBody().string());
        }
    }

    private ArticlesList mapA(Response<ArticlesList> response) throws IOException {
        if (response == null) throw new IllegalStateException("Invalid lookups response");

        //LookupsEntity entity = response.body();
        if (response.body() == null) throw new IllegalStateException("Invalid articles response");

        switch (response.code()) {
            case 200:
                 return response.body();
            default:
                if (TextUtils.isEmpty(response.errorBody().string())) {
                    throw new IllegalStateException("Fetch articles failure");
                }
                throw new IllegalStateException(response.errorBody().string());
        }
    }

    private ArticlesList mapB(Response<ArticlesList> response) throws IOException {
        if (response == null) throw new IllegalStateException("Invalid articles response");
        if (response.body() == null) throw new IllegalStateException("Invalid articles response");

        switch (response.code()) {
            case 200:
                return response.body();
            default:
                if (TextUtils.isEmpty(response.errorBody().string())) {
                    throw new IllegalStateException("Fetch articles failure");
                }
                throw new IllegalStateException(response.errorBody().string());
        }
    }
    ArticlesList articlesA;
    ArticlesList articlesB;

    @Override
    public Single<ArticlesList> fetchArticlesA() {
        return Single.wrap(this.articlesApiService.fetchArticlesA("the-next-web", "533af958594143758318137469b41ba9").subscribeOn(Schedulers.io()))
                .map(response -> {
                    articlesA = this.mapA(response);
                    //this.cacheArticles(articlesB);
                    return articlesA;
                });
    }

    @Override
    public Single<ArticlesList> fetchArticlesB() {
        return Single.wrap(this.articlesApiService.fetchArticlesB("associated-press", "533af958594143758318137469b41ba9")
                .subscribeOn(Schedulers.io()))
                .map(response -> {
                    articlesB = this.mapB(response);
                    //this.cacheArticles(articlesB);
                    return articlesB;
                });
    }


    ArticlesList list;

    @Override
    public  Single<ArticlesList> fetchArticles() {
        Single<ArticlesList> articlesA = fetchArticlesA();
        Single<ArticlesList> articlesB = fetchArticlesB();
        return Single.zip(articlesA, articlesB, new BiFunction<ArticlesList, ArticlesList, ArticlesList>() {
            @Override
            public ArticlesList apply(@NonNull ArticlesList s, @NonNull ArticlesList s2) throws Exception {
                s.getArticles().addAll(s2.getArticles());
                return s;
            }
        });
    }
}
