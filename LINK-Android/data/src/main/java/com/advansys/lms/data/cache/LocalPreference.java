package com.horus.link.data.cache;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.bioteamit.support.security.obfuscation.Obfuscation;

public class LocalPreference {

    public static final String PREFERENCES_NAME = LocalPreference.class.getPackage().getName();

    private static final String PREF_AUTHORIZATION_TOKEN
            = "DC98B1C1-8FF9-4751-A4D2-58DAAE3A7B7D";
    private static final String PREF_LOGGING_MODE
            = "EDEF1518-AE97-43EE-B468-49D81ED3D6CE";
    private static final String PREF_LOGGING_LEVEL
            = "BC477F87-B724-46E5-B8C1-F34473BA6620";

    private static final String DEFAULT_AUTHORIZATION_TOKEN = null;
    private static final String DEFAULT_LOGGING_MODE = "ALL";
    private static final String DEFAULT_LOGGING_LEVEL = "VERBOSE";

    private final SharedPreferences preferences;
    private final Obfuscation obfuscation;

    public LocalPreference(final Context context, Obfuscation obfuscation) {
        this.preferences = context.getApplicationContext()
                .getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        this.obfuscation = obfuscation;
    }

    public String getAuthorizationToken() {
        String obfuscatedValue = this.preferences.getString(PREF_AUTHORIZATION_TOKEN, null);
        if (TextUtils.isEmpty(obfuscatedValue)) return DEFAULT_AUTHORIZATION_TOKEN;

        return this.obfuscation.deobfuscate(obfuscatedValue);
    }

    public void setAuthorizationToken(String value) {
        if (TextUtils.isEmpty(value)) {
            this.preferences.edit().remove(PREF_AUTHORIZATION_TOKEN).apply();
        } else {
            String obfuscatedValue = this.obfuscation.obfuscate(value);
            this.preferences.edit().putString(PREF_AUTHORIZATION_TOKEN, obfuscatedValue).apply();
        }
    }

    public String getLoggingMode() {
        String obfuscatedValue = this.preferences.getString(PREF_LOGGING_MODE, null);
        if (TextUtils.isEmpty(obfuscatedValue)) return DEFAULT_LOGGING_MODE;

        return this.obfuscation.deobfuscate(obfuscatedValue);
    }

    public void setLoggingMode(String value) {
        if (TextUtils.isEmpty(value)) {
            this.preferences.edit().remove(PREF_LOGGING_MODE).apply();
        } else {
            String obfuscatedValue = this.obfuscation.obfuscate(value);
            this.preferences.edit().putString(PREF_LOGGING_MODE, obfuscatedValue).apply();
        }
    }

    public String getLoggingLevel() {
        String obfuscatedValue = this.preferences.getString(PREF_LOGGING_LEVEL, null);
        if (TextUtils.isEmpty(obfuscatedValue)) return DEFAULT_LOGGING_LEVEL;

        return this.obfuscation.deobfuscate(obfuscatedValue);
    }

    public void setLoggingLevel(String value) {
        if (TextUtils.isEmpty(value)) {
            this.preferences.edit().remove(PREF_LOGGING_LEVEL).apply();
        } else {
            String obfuscatedValue = this.obfuscation.obfuscate(value);
            this.preferences.edit().putString(PREF_LOGGING_LEVEL, obfuscatedValue).apply();
        }
    }
}
