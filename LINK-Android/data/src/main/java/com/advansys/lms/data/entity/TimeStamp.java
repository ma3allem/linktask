package com.horus.link.data.entity;

import com.google.common.base.Optional;

public interface TimeStamp {

    Optional<String> createdTime();

    Optional<String> modifiedTime();

    interface Creator<T> {

        T createdTime(Optional<String> createdTime);

        T modifiedTime(Optional<String> modifiedTime);
    }
}
