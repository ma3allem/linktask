package com.horus.link.mvp.vp.articlesDetails;

import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;

import com.advansys.lms.domain.model.articles.ArticlesList;
import com.advansys.lms.domain.usecase.FetchArticlesUseCase;
import com.bioteamit.support.android.device.mvp.ui.Contract;
import com.bioteamit.support.android.device.mvp.ui.Presenter;
import com.horus.link.data.cache.LocalPreference;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public final class ArticlesDetailsPresenter extends Presenter<ArticlesDetailsActivity>
        implements ArticlesDetailsContract.ArticlesDetailsPresenter, Contract.Lifecycle {

    private final LocalPreference preference;
    private final FetchArticlesUseCase fetchArticlesUseCase;

    @Inject
    public ArticlesDetailsPresenter(LocalPreference preference,
                                    FetchArticlesUseCase fetchArticlesUseCase) {
        this.preference = preference;
        this.fetchArticlesUseCase = fetchArticlesUseCase;
    }

    @Override
    public void initialize(Bundle bundle) {
    }

    @Override
    public void saveInstance(Bundle bundle) {
    }

    @Override
    public void updateConfiguration(Configuration configuration) {
    }

    @Override
    public void start() {
    }

   @Override
   public void resume() {
   }

    @Override
    public void pause() {
    }

    @Override
    public void stop() {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void loadArticleDetails(int id) {
        char []x = {};
    }

}
