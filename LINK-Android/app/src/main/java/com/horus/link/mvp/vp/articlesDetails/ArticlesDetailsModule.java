package com.horus.link.mvp.vp.articlesDetails;

import android.content.Context;

import com.advansys.lms.data.net.api.ArticlesApiService;
import com.advansys.lms.data.repository.ArticlesDataRepository;
import com.advansys.lms.domain.repository.ArticlesRepository;
import com.advansys.lms.domain.usecase.FetchArticlesUseCase;
import com.horus.link.data.cache.LocalPreference;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ArticlesDetailsModule {

    private ArticlesDetailsActivity activity;

    public ArticlesDetailsModule(ArticlesDetailsActivity activity) {
        this.activity = activity;
    }

    @Provides
    public Context provideContext() {
        return this.activity;
    }

    @Provides
    public ArticlesApiService provideLookupApiService(Retrofit retrofit) {
        return retrofit.create(ArticlesApiService.class);
    }

    @Provides
    public ArticlesRepository provideLookupRepository(
            ArticlesApiService apiService) {
        return new ArticlesDataRepository(apiService);
    }

    @Provides
    public FetchArticlesUseCase provideFetchLookupsUseCase(
            ArticlesRepository articlesRepository) {
        return new FetchArticlesUseCase(articlesRepository);
    }


    @Provides
    public ArticlesDetailsPresenter provideLoginPresenter(LocalPreference preference,
                                                FetchArticlesUseCase fetchArticlesUseCase) {
        return new ArticlesDetailsPresenter(preference, fetchArticlesUseCase);
    }
}
