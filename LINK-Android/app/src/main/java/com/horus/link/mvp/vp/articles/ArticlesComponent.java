package com.horus.link.mvp.vp.articles;

import com.bioteamit.support.android.device.injection.PerActivity;
import com.horus.link.application.LinkComponent;

import dagger.Component;

@PerActivity
@Component(dependencies = LinkComponent.class, modules = {ArticlesModule.class})
public interface ArticlesComponent {
    void inject(ArticlesActivity activity);
}
