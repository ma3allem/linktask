package com.horus.link.views.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.advansys.lms.domain.model.articles.Article;
import com.horus.link.R;
import com.horus.link.application.LinkApplication;
import com.horus.link.listeners.ArticlesAdapterListener;
import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ArticlesViewHolder> {

    int selectedIndex = -1 ;
    private List<Article> articlesCollection;
    private final LayoutInflater layoutInflater;
    int row_index = -1;

    private ArticlesAdapterListener onItemClickListener;

    Context ctx;

    @Inject
    public ArticlesAdapter(Context context/*, @NonNull HomeFragment _view*/) {
        ctx = context;
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.articlesCollection = Collections.emptyList();
    }

    @Override public int getItemCount() {
        return (this.articlesCollection != null) ? this.articlesCollection.size() : 0;
    }

    @Override public ArticlesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.row_articles, parent, false);
        return new ArticlesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ArticlesViewHolder holder, final int position) {

        Article article = articlesCollection.get(position);
        holder.title.setText(article.getTitle().trim());
        holder.by.setText("By " + article.getAuthor().trim());
        holder.date.setText("April 3, 2017");

        Picasso.with(ctx).load(article.getUrlToImage()).placeholder(R.drawable.placeholder).into(holder.avatar);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.oArticleClicked(article);
            }
        });

    }

    @Override public long getItemId(int position) {
        return position;
    }

    public void setTabCollection(Collection<Article> _tabCollection) {
        this.articlesCollection =  (List<Article>) _tabCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener (ArticlesAdapterListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    static class ArticlesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar)
        ImageView avatar;
        @BindView(R.id.title) TextView title;
        @BindView(R.id.by) TextView by;
        @BindView(R.id.date) TextView date;

        ArticlesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}