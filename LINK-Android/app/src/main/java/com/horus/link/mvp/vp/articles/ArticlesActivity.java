package com.horus.link.mvp.vp.articles;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.advansys.lms.domain.model.articles.Article;
import com.advansys.lms.domain.model.articles.ArticlesList;
import com.bioteamit.support.android.device.injection.DaggerService;
import com.bioteamit.support.android.device.mvp.ui.BaseActivity;
import com.google.gson.Gson;
import com.horus.link.R;
import com.horus.link.application.LinkApplication;
import com.horus.link.application.LinkComponent;
import com.horus.link.application.LinkModule;
import com.horus.link.listeners.ArticlesAdapterListener;
import com.horus.link.mvp.vp.articlesDetails.ArticlesDetailsActivity;
import com.horus.link.views.adapters.ArticlesAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticlesActivity extends BaseActivity<ArticlesPresenter> implements ArticlesContract.ArticlesView, ArticlesAdapterListener,
        NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.pb_progress)
    protected View progressView;
    @BindView(R.id.articles_form)
    protected View articlesFormView;
    @BindView(R.id.rv_articles)
    RecyclerView rvArticles;

    @Inject
    ArticlesAdapter adapter;

    private DrawerLayout drawerMain;
    private NavigationView navigationViewMAin;
    ImageView iv_drawer_menu;
    boolean drawer_open = false;

    ArticlesActivity act;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.configureScope();
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_articles);
        ButterKnife.bind(this);

        this.populateAutoComplete();

        act = this;
        setupDrawer();
        setupRecycler();
        this.loadArticles();
    }


    @Override
    public void warnInternetConnection() {
        this.runOnUiThread(() -> {
            Snackbar.make(this.findViewById(R.id.articles_form), R.string.no_internet_connection, Snackbar.LENGTH_SHORT).show();
        });
    }

    @Override
    public void showProgress() {
        this.showProgress(true);
    }

    @Override
    public void hideProgress() {
        this.showProgress(false);
    }


    @Override
    public void errorInitializationFailure(String message) {
        this.runOnUiThread(() -> {
            this.hideProgress();
        });
    }

    @Override
    public void setupDrawer() {
        drawerMain = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerMain, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerMain.setDrawerListener(toggle);
        toggle.syncState();

        navigationViewMAin = (NavigationView) findViewById(R.id.nav_view);
        navigationViewMAin.setNavigationItemSelectedListener(this);
        navigationViewMAin.post(new Runnable() {
            @Override
            public void run() {
                navigationViewMAin.getMenu().getItem(0).setChecked(true);
            }
        });

        iv_drawer_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_drawer_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerMain.isDrawerOpen(Gravity.START)) {
                    drawerMain.closeDrawer(Gravity.START);
                } else {
                    drawerMain.openDrawer(Gravity.START);
                }
            }
        });
    }

    @Override
    public void setupRecycler() {
        //ArticlesAdapter
        adapter.setOnItemClickListener(this);
        rvArticles.setAdapter(adapter);
        rvArticles.setLayoutManager(new LinearLayoutManager(this));
    }


    protected void configureScope() {
        DaggerArticlesComponent.builder()
                .linkComponent(DaggerService.createComponent(LinkComponent.class,
                        new LinkModule((LinkApplication) this.getApplication())))
                .articlesModule(new ArticlesModule(this)).build()
                .inject(this);
    }

    private void populateAutoComplete() {
    }

    private void loadArticles() {
        this.hideSoftInput();
        this.showProgress(true);
        this.presenter.loadArticles();
    }

    private void hideSoftInput() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        View view = this.getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        this.runOnUiThread(() -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                this.showProgressApi13(show);
            } else {
                this.progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                this.articlesFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });
    }

    private void showProgressApi13(boolean show) {
        int shortAnimTime = this.getResources().getInteger(android.R.integer.config_shortAnimTime);

        this.articlesFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        this.articlesFormView.animate()
                .setDuration(shortAnimTime)
                .alpha(show ? 0 : 1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animator) {
                        ArticlesActivity.this.articlesFormView
                                .setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });

        this.progressView.setVisibility(show ? View.VISIBLE : View.GONE);
        this.progressView.animate()
                .setDuration(shortAnimTime)
                .alpha(show ? 1 : 0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ArticlesActivity.this.progressView
                                .setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
    }

    private void navigateToArticleDetails(Article article) {
        Snackbar.make(articlesFormView, article.getTitle(), Snackbar.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ArticlesDetailsActivity.class);
        intent.putExtra("article", new Gson().toJson(article));
        navigateToActivity(ArticlesDetailsActivity.class, intent);
    }

    private void navigateToActivity(Class aClass, Intent intent) {
        this.finish();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            this.startActivity(intent, ActivityOptions.makeCustomAnimation(this,
                    android.R.anim.slide_in_left, android.R.anim.slide_out_right).toBundle());
        } else {
            this.startActivity(intent);
            this.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
    }


    @Override
    public void renderArticles(ArticlesList articles)
    {
        adapter.setTabCollection(articles.getArticles());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void oArticleClicked(Article article) {
        navigateToArticleDetails(article);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        //ShowSnackbar(((TextView) navigationViewMAin.findViewById(R.id.tab)).getText().toString());
        switch (item.getItemId()) {
            case R.id.nav_explore: {
                ShowSnackbar("Explore");
                LinearLayout lin_ = (LinearLayout) findViewById(R.id.lin_explore);
                lin_.setVisibility(View.VISIBLE);

                LinearLayout lin_explore = (LinearLayout) findViewById(R.id.lin_magazine);
                lin_explore.setVisibility(View.INVISIBLE);
                LinearLayout lin_magazine = (LinearLayout) findViewById(R.id.lin_gallery);
                lin_magazine.setVisibility(View.INVISIBLE);
                LinearLayout lin_live = (LinearLayout) findViewById(R.id.lin_wish);
                lin_live.setVisibility(View.INVISIBLE);
                LinearLayout lin_gallery = (LinearLayout) findViewById(R.id.lin_live);
                lin_gallery.setVisibility(View.INVISIBLE);
                toggleDrawer();
                break;
            }
            case R.id.nav_e_magazine: {
                ShowSnackbar("E-Magazine");
                LinearLayout lin_ = (LinearLayout) findViewById(R.id.lin_magazine);
                lin_.setVisibility(View.VISIBLE);

                LinearLayout lin_explore = (LinearLayout) findViewById(R.id.lin_explore);
                lin_explore.setVisibility(View.INVISIBLE);
                LinearLayout lin_magazine = (LinearLayout) findViewById(R.id.lin_gallery);
                lin_magazine.setVisibility(View.INVISIBLE);
                LinearLayout lin_live = (LinearLayout) findViewById(R.id.lin_wish);
                lin_live.setVisibility(View.INVISIBLE);
                LinearLayout lin_gallery = (LinearLayout) findViewById(R.id.lin_live);
                lin_gallery.setVisibility(View.INVISIBLE);
                toggleDrawer();
                break;
            }
            case R.id.nav_gallery: {
                ShowSnackbar("Gallery");
                LinearLayout lin_ = (LinearLayout) findViewById(R.id.lin_gallery);
                lin_.setVisibility(View.VISIBLE);

                LinearLayout lin_explore = (LinearLayout) findViewById(R.id.lin_explore);
                lin_explore.setVisibility(View.INVISIBLE);
                LinearLayout lin_magazine = (LinearLayout) findViewById(R.id.lin_magazine);
                lin_magazine.setVisibility(View.INVISIBLE);
                LinearLayout lin_live = (LinearLayout) findViewById(R.id.lin_wish);
                lin_live.setVisibility(View.INVISIBLE);
                LinearLayout lin_gallery = (LinearLayout) findViewById(R.id.lin_live);
                lin_gallery.setVisibility(View.INVISIBLE);
                toggleDrawer();
                break;
            }
            case R.id.nav_live_chat: {
                ShowSnackbar("Live Chat");
                LinearLayout lin_ = (LinearLayout) findViewById(R.id.lin_live);
                lin_.setVisibility(View.VISIBLE);

                LinearLayout lin_explore = (LinearLayout) findViewById(R.id.lin_explore);
                lin_explore.setVisibility(View.INVISIBLE);
                LinearLayout lin_magazine = (LinearLayout) findViewById(R.id.lin_magazine);
                lin_magazine.setVisibility(View.INVISIBLE);
                LinearLayout lin_live = (LinearLayout) findViewById(R.id.lin_wish);
                lin_live.setVisibility(View.INVISIBLE);
                LinearLayout lin_gallery = (LinearLayout) findViewById(R.id.lin_gallery);
                lin_gallery.setVisibility(View.INVISIBLE);
                toggleDrawer();
                break;
            }

            case R.id.nav_wish_list: {
                ShowSnackbar("Wish List");
                LinearLayout lin_ = (LinearLayout) findViewById(R.id.lin_wish);
                lin_.setVisibility(View.VISIBLE);

                LinearLayout lin_explore = (LinearLayout) findViewById(R.id.lin_explore);
                lin_explore.setVisibility(View.INVISIBLE);
                LinearLayout lin_magazine = (LinearLayout) findViewById(R.id.lin_magazine);
                lin_magazine.setVisibility(View.INVISIBLE);
                LinearLayout lin_live = (LinearLayout) findViewById(R.id.lin_live);
                lin_live.setVisibility(View.INVISIBLE);
                LinearLayout lin_gallery = (LinearLayout) findViewById(R.id.lin_gallery);
                lin_gallery.setVisibility(View.INVISIBLE);
                toggleDrawer();
                break;
            }

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void toggleDrawer()
    {
        if (drawer_open == false) {
            drawerMain.openDrawer(Gravity.START); //Edit Gravity.START need API 14
            drawer_open = true;
        } else {
            drawerMain.closeDrawer(Gravity.START); //Edit Gravity.START need API 14
            drawer_open = false;
        }
    }

    @Override
    public void ShowSnackbar(String message)
    {
        Snackbar.make(articlesFormView, message, Snackbar.LENGTH_SHORT).show();
    }
}
