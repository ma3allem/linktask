package com.horus.link.mvp.vp.articlesDetails;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.advansys.lms.domain.model.articles.Article;
import com.bioteamit.support.android.device.injection.DaggerService;
import com.bioteamit.support.android.device.mvp.ui.BaseActivity;
import com.google.gson.Gson;
import com.horus.link.R;
import com.horus.link.application.LinkApplication;
import com.horus.link.application.LinkComponent;
import com.horus.link.application.LinkModule;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArticlesDetailsActivity extends BaseActivity<ArticlesDetailsPresenter> implements ArticlesDetailsContract.ArticlesDetailsView {

    @BindView(R.id.articles_form)
    protected View articlesFormView;

    @BindView(R.id.iv_avatar)
    ImageView ivAvatar;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.tv_by)
    TextView tvBy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.configureScope();
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_articles_details);
        ButterKnife.bind(this);
        this.populateAutoComplete();
        initialize();
    }


    @Override
    public void warnInternetConnection() {
        this.runOnUiThread(() -> {
            Snackbar.make(this.findViewById(R.id.articles_form), R.string.no_internet_connection, Snackbar.LENGTH_SHORT).show();
        });
    }

    protected void configureScope() {
        DaggerArticlesDetailsComponent.builder()
                .linkComponent(DaggerService.createComponent(LinkComponent.class,
                        new LinkModule((LinkApplication) this.getApplication())))
                .articlesDetailsModule(new ArticlesDetailsModule(this)).build()
                .inject(this);
    }

    private void populateAutoComplete() {
    }

    private void hideSoftInput() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        View view = this.getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    Article article;

    @Override
    public void initialize()
    {
        article = new Gson().fromJson(getIntent().getExtras().getString("article"), Article.class);
        tvBy.setText(article.getAuthor());
        tvDesc.setText(article.getDescription());
        tvTitle.setText(article.getTitle());
        Picasso.with(this).load(article.getUrlToImage()).into(ivAvatar);
    }


    @OnClick(R.id.bt_website)
    public void GoToWebsite()
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(article.getUrl()));
        startActivity(browserIntent);
    }

    @OnClick(R.id.iv_back)
    public void GoBack()
    {
        onBackPressed();
    }

    private void navigateToActivity(Class aClass) {
        Intent intent = new Intent(this, aClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.finish();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            this.startActivity(intent, ActivityOptions.makeCustomAnimation(this,
                    android.R.anim.fade_in, android.R.anim.fade_out).toBundle());
        } else {
            this.startActivity(intent);
            this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }
}
