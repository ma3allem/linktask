package com.horus.link.mvp.vp.articles;

import com.advansys.lms.domain.model.articles.ArticlesList;
import com.bioteamit.support.android.device.mvp.ui.Contract;

public interface ArticlesContract extends Contract {

    interface ArticlesView extends View {
        void warnInternetConnection();
        void showProgress();
        void hideProgress();
        void errorInitializationFailure(String message);

        void setupDrawer();
        void setupRecycler();
        void renderArticles(ArticlesList articles);

        void toggleDrawer();
        void ShowSnackbar(String message);
    }

    interface ArticlesPresenter extends Presenter {
        void loadArticles();
    }
}
