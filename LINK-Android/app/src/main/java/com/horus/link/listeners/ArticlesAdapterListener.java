package com.horus.link.listeners;


import com.advansys.lms.domain.model.articles.Article;

public interface ArticlesAdapterListener {
    void oArticleClicked(Article article);
}