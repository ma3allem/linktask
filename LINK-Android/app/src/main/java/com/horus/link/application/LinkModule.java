package com.horus.link.application;

import android.content.Context;

import com.horus.link.data.cache.LocalPreference;
import com.horus.link.data.entity.OptionalAdapterFactory;
import com.horus.link.data.net.client.HttpClient;
import com.bioteamit.support.android.device.mvp.app.ActivityLifecycle;
import com.bioteamit.support.security.obfuscation.Obfuscation;
import com.bioteamit.support.security.obfuscation.android.Base64Obfuscation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module()
public class LinkModule {

    //private static final String BASE_URL = "http://52.7.119.210:5040";
    private static final String BASE_URL = "https://newsapi.org/";

    private LinkApplication application;

    public <T extends LinkApplication> LinkModule(T application) {
        this.application = application;
    }

    @Provides
    public Context provideContext() {
        return this.application;
    }

    @Provides
    @Singleton
    public Thread.UncaughtExceptionHandler provideUncaughtExceptionHandler() {
        return Thread.getDefaultUncaughtExceptionHandler();
    }

    @Provides
    @Singleton
    public ActivityLifecycle provideActivityLifecycle() {
        return new ActivityLifecycle();
    }

    @Provides
    @Singleton
    public LinkPresenter provideLinkPresenter(
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler,
            ActivityLifecycle activityLifecycle) {
        return new LinkPresenter(uncaughtExceptionHandler, activityLifecycle);
    }

    @Provides
    @Singleton
    public Obfuscation provideObfuscation() {
        return new Base64Obfuscation();
    }

    @Provides
    @Singleton
    public LocalPreference provideLocalPreference(Context context, Obfuscation obfuscation) {
        return new LocalPreference(context, obfuscation);
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new GsonBuilder()
                .registerTypeAdapterFactory(OptionalAdapterFactory.create())
                //.registerTypeAdapterFactory(AutoValueAdapterFactory.create())
                .setPrettyPrinting()
                .create();
    }

    @Provides
    @Singleton
    public HttpClient provideHttpClient(LocalPreference preference) {
        return new HttpClient(preference);
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(HttpClient httpClient) {
        return httpClient.createOkHttpClient();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient httpClient, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }
}
