package com.horus.link.mvp.vp.articlesDetails;

import com.bioteamit.support.android.device.injection.PerActivity;
import com.horus.link.application.LinkComponent;
import com.horus.link.mvp.vp.articlesDetails.ArticlesDetailsActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = LinkComponent.class, modules = {ArticlesDetailsModule.class})
public interface ArticlesDetailsComponent {
    void inject(ArticlesDetailsActivity activity);
}
