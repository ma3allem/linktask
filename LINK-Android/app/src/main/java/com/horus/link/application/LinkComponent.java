package com.horus.link.application;

import com.horus.link.data.cache.LocalPreference;
import com.bioteamit.support.security.obfuscation.Obfuscation;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = LinkModule.class)
public interface LinkComponent {

    void inject(LinkApplication application);

    Obfuscation provideObfuscation();

    LocalPreference provideLocalPreference();

    Retrofit provideRetrofit();
}
