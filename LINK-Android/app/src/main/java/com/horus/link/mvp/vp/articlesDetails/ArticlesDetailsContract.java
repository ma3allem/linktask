package com.horus.link.mvp.vp.articlesDetails;

import com.bioteamit.support.android.device.mvp.ui.Contract;

public interface ArticlesDetailsContract extends Contract {

    interface ArticlesDetailsView extends View {
        void warnInternetConnection();
        void initialize();
    }

    interface ArticlesDetailsPresenter extends Presenter {
        void loadArticleDetails(int id);
    }
}
