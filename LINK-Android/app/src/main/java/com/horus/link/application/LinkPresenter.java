package com.horus.link.application;

import com.bioteamit.support.android.device.mvp.app.ActivityLifecycle;
import com.bioteamit.support.android.device.mvp.app.Presenter;

import javax.inject.Inject;

public final class LinkPresenter extends Presenter {

    @Inject
    public LinkPresenter(Thread.UncaughtExceptionHandler uncaughtExceptionHandler,
                        ActivityLifecycle activityLifecycle) {
        super(uncaughtExceptionHandler, activityLifecycle);
    }
}
