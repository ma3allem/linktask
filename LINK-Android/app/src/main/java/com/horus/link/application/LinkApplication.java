package com.horus.link.application;

import com.bioteamit.support.android.device.injection.DaggerService;
import com.bioteamit.support.android.device.mvp.app.BaseApplication;

public final class LinkApplication extends BaseApplication<LinkPresenter> {



    @Override
    public void onCreate() {
        DaggerService.createComponent(LinkComponent.class, new LinkModule(this))
                .inject(this);
        super.onCreate();
    }


}
