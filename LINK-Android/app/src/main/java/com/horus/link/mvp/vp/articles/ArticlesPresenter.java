package com.horus.link.mvp.vp.articles;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import com.advansys.lms.domain.model.articles.ArticlesList;
import com.advansys.lms.domain.usecase.FetchArticlesUseCase;
import com.bioteamit.support.android.device.mvp.ui.Contract;
import com.bioteamit.support.android.device.mvp.ui.Presenter;
import com.horus.link.data.cache.LocalPreference;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public final class ArticlesPresenter extends Presenter<ArticlesActivity>
        implements ArticlesContract.ArticlesPresenter, Contract.Lifecycle {

    private final LocalPreference preference;
    private final FetchArticlesUseCase fetcharticlesUseCase;

    @Inject
    public ArticlesPresenter(LocalPreference preference,
                          FetchArticlesUseCase fetchArticlesUseCase) {
        this.preference = preference;
        this.fetcharticlesUseCase = fetchArticlesUseCase;
    }

    @Override
    public void initialize(Bundle bundle) {
    }

    @Override
    public void saveInstance(Bundle bundle) {
    }

    @Override
    public void updateConfiguration(Configuration configuration) {
    }

    @Override
    public void start() {
    }

    @Override
    public void resume() {
    }

    //@Override
    //public void resume() {
    //    this.getView().showProgress();
    //    this.fetcharticlesUseCase.execute(null)
    //            .subscribeOn(AndroidSchedulers.mainThread())
    //            .subscribe(response -> this.onFetchArticlesSuccess(response),
    //                    throwable -> this.onFetchLookupsFailure(throwable));
    //}

    @Override
    public void pause() {
    }

    @Override
    public void stop() {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void loadArticles() {
        //Credentials credentials = new Credentials(email, password);
        this.fetcharticlesUseCase.execute(null)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> this.onFetchArticlesSuccess(response),
                       throwable -> this.onFetchLookupsFailure(throwable));
    }

    private void onFetchArticlesSuccess(ArticlesList articles) {
        this.getView().hideProgress();
        getView().renderArticles(articles);
    }

    private void onFetchLookupsFailure(Throwable throwable) {
        System.out.println("Ret articles fail " );
        this.getView().errorInitializationFailure(throwable.getMessage());
    }


}
