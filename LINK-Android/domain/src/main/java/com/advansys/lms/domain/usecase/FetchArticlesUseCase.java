package com.advansys.lms.domain.usecase;

import com.advansys.lms.domain.model.articles.ArticlesList;
import com.advansys.lms.domain.repository.ArticlesRepository;
import com.bioteamit.support.domain.UseCase;
import com.bioteamit.support.util.Preconditions;

import javax.inject.Inject;

import io.reactivex.Single;

public class FetchArticlesUseCase implements UseCase<Void, ArticlesList> {

    private final ArticlesRepository articlesRepository;

    private ArticlesList articles;

    @Inject
    public FetchArticlesUseCase(ArticlesRepository articlesRepository) {
        Preconditions.checkArgument(articlesRepository != null,
                "Articles repository must not be null");
        this.articlesRepository = articlesRepository;
    }

    @Override
    public Void parameter() {
        return null;
    }

    @Override
    public ArticlesList response() {
        return this.articles;
    }

    @Override
    public Single<ArticlesList> execute(Void aVoid) {

        //Single<ArticlesList> ArticlesListA = makeRequestToServiceA();
        //Single<ArticlesList> ArticlesListB = makeRequestToServiceB();

        return articlesRepository.fetchArticles();

       //Single.just(ArticlesListA)
       //         .mergeWith(Single.just(ArticlesListB))
       //         .subscribe(new Subscriber<Single<ArticlesList>>() {
       //             @Override
       //             public void onSubscribe(Subscription s) {
//
       //             }
//
       //             @Override
       //             public void onNext(Single<ArticlesList> articlesListSingle) {
//
       //             }
//
       //             @Override
       //             public void onError(Throwable t) {
//
       //             }
//
       //             @Override
       //             public void onComplete() {
//
       //             }
       //         });
//
        //return Single.merge(ArticlesListA, ArticlesListB);/
                //.ignoreElements()
                //.observeOn(AndroidSchedulers.mainThread())
                //.doOnComplete(() -> { /* show alert */ })
                //.subscribe();

        //return this.articlesRepository.fetchArticles();

        //return Single.merge(makeRequestToServiceA(), makeRequestToServiceB()).doOnComplete(() -> { /* show alert */ })

        //Single
        //        .merge(makeRequestToServiceA(), makeRequestToServiceA())
        //        .ignoreElements()
        //        //.observeOn(AndroidSchedulers.mainThread())
        //        .doOnComplete(() -> { /* show alert */ })
        //        .subscribe();


        //Completable completable = Single.zip(makeRequestToServiceA(), makeRequestToServiceA(), (aLong, aLong2) -> aLong)
        //        .toCompletable();

        //TestObserver<Void> test = completable.test();
        //return completable.compl

        //makeRequestToServiceA()
        //        .flatMap(new Func1<ArticlesList, Single<? extends String>>() {
        //            @Override
        //            public Single<? extends String> call(String responseFromServiceA) {
        //                //make second request based on response from ServiceA
        //                return makeRequestToServiceB(responseFromServiceA);
        //            }
        //        }, new Func2<String, String, Single<String>>() {
        //            @Override
        //            public Observable<String> call(String responseFromServiceA, String responseFromServiceB) {
        //                //combine results
        //                return Observable.just("here is combined result!");
        //            }
        //        })
        //apply schedulers, subscribe etc
    }

    private Single<ArticlesList> makeRequestToServiceA() {
        //return Single.just(null); //some network call based on response from ServiceA
        return this.articlesRepository.fetchArticlesA();
    }

    private Single<ArticlesList> makeRequestToServiceB() {
        //return Single.just(null); //some network call based on response from ServiceA
        return this.articlesRepository.fetchArticlesB();
    }

    //@Override
    //public Single<ArticlesList> execute() {
    //    return this.articlesRepository.fetchArticles();
    //}
}
