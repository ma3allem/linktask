package com.advansys.lms.domain.repository;
import com.advansys.lms.domain.model.articles.ArticlesList;

import io.reactivex.Single;

public interface ArticlesRepository {

    Single<ArticlesList> fetchArticlesA();

    Single<ArticlesList> fetchArticlesB();

    Single<ArticlesList> fetchArticles();

}
